class Particle:
    def __init__(self, location):
        self.location = location

    def checkAround(location, squareSize, matrix):
        print
        foundFriend = False  # found another particle
        nearEdge = (location[1] + 1) > squareSize - 1 or location[1] < 2 or (location[0] + 1) > squareSize - 1 or location[0] < 2
        if not nearEdge:
            try:
                newlist.append(dlist[1])
            except IndexError:
                pass
            if matrix[location[1] + 1, location[0]] == 1:
                foundFriend = True
            if (matrix[location[1] - 1, location[0]]) == 1:
                foundFriend = True
            if (matrix[location[1], location[0] + 1]) == 1:
                foundFriend = True
            if (matrix[location[1], location[0] - 1]) == 1:
                foundFriend = True
        if not foundFriend and not nearEdge:
            location = [location[0] + random.randint(-1, 1), location[1] + random.randint(-1, 1)]
        return (location, foundFriend, nearEdge)

    def onEdge(self):
        if self.rect.left <= 0:
            self.vector = (abs(self.vector[0]), self.vector[1])
        elif self.rect.top <= 0:
            self.vector = (self.vector[0], abs(self.vector[1]))
        elif self.rect.right >= WINDOWSIZE:
            self.vector = (-abs(self.vector[0]), self.vector[1])
        elif self.rect.bottom >= WINDOWSIZE:
            self.vector = (self.vector[0], -abs(self.vector[1]))

    def update(self):
        if freeParticles in self.groups():
            self.surface.fill((0, 0, 0), self.rect)
            self.remove(freeParticles)
            if pygame.sprite.spritecollideany(self, freeParticles):
                self.accelerate((randint(-MAXSPEED, MAXSPEED), randint(-MAXSPEED, MAXSPEED)))
                self.add(freeParticles)
            elif pygame.sprite.spritecollideany(self, tree):
                self.stop()
            else:
                self.add(freeParticles)
            self.onEdge()
            if (self.vector == (0, 0)) and tree not in self.groups():
                self.accelerate((randint(-MAXSPEED, MAXSPEED), randint(-MAXSPEED, MAXSPEED)))
            self.rect.move_ip(self.vector[0], self.vector[1])
        self.surface.fill(COLOR, self.rect)

    def stop(self):
        self.vector = (0, 0)
        self.remove(freeParticles)
        self.add(tree)

    def accelerate(self, vector):
        self.vector = vector
