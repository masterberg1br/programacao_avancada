"""
This functions checks the initial locations, to make sure it is not at the edge of the square.
If not at the edge:
checks the perimeter (up, down, left, right). The boolean variables foundFriend, exitCircle, nearEdge
are then assigned as TRUE, if they are true.
If the particle is not next to the other, and not at the edge of the square the new location is defined

PARAMETERS:
INPUT = location, in form [X,Y]
OUTPUT = new location ([X,Y]), foundFriend (BOOLEAN), nearEdge (BOOLEAN), exitCircle(BOOLEAN)

"""

import contextlib
import random
import numpy as np


def checkAround(location, squareSize, matrix):
    print(location, squareSize)
    foundFriend = False  # found another particle
    nearEdge = (location[1]) > squareSize or location[1] < 2 or (location[0]) > squareSize or location[0] < 2

    if not nearEdge:
        with contextlib.suppress(IndexError):
            if matrix[location[1] + 1, location[0]] == 1:
                foundFriend = True

            if (matrix[location[1] - 1, location[0]]) == 1:
                foundFriend = True

            if (matrix[location[1], location[0] + 1]) == 1:
                foundFriend = True

            if (matrix[location[1], location[0] - 1]) == 1:
                foundFriend = True

    if not foundFriend and not nearEdge:
        location = [location[0] + random.randint(-1, 1), location[1] + random.randint(-1, 1)]

    return (location, foundFriend, nearEdge)
