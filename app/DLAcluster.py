"""
This is the main funciton for the DLA cluster model.
INPUT: RADIUS (Integer), needGif (Boolean)
OUTPUT: # of particles in the cluster (int), resulting matrix (populated by 0, 1, 2)
SAVED OUTPUT: in the folder images saves the resulting cluster image and .gif file
    -note: if folder images does not exist, it is created first
"""

import itertools
import random
import numpy
import matplotlib.pyplot as plt
import os
from matplotlib import colors


############ Custom functions
from checkAround import checkAround

############


def DLAcluster(radius):

    # check if folder "images" exists, and if not - create it
    if not os.path.isdir("images"):
        os.mkdir("images")

    # initialize variables that are dependent upon the radius
    # note - we add 2 to the parameters to get a thick broder between the edges of the disk and square
    # x coordinate of a seed particle
    seedX = radius + 1
    # y coordinate of a seed
    seedY = radius + 1
    # size of the grid to account for field of wandering around
    squareSize = radius * 2 + 2

    matrix = numpy.zeros((squareSize, squareSize))

    for row, col in itertools.product(range(squareSize), range(squareSize)):
        # put a seed particle
        if row == seedY and col == seedX:
            matrix[row][col] = 1
        # define field outside of circle
        elif numpy.sqrt((seedX - col) ** 2 + (seedY - row) ** 2) > radius:
            matrix[row][col] = 2

    # Initialize the random walker counter
    randomWalkersCount = 0

    # Set the cluster to NOT be complete
    completeCluster = False

    # Start running random walkers
    addedCount = 0  # keep track of number added

    while not completeCluster:
        # Release a walker
        randomWalkersCount += 1

        # Generate a (Xstart, Ystart) for walker, need within radius
        theta = 2 * numpy.pi * random.random()  # generate random theta
        location = [int(radius * numpy.cos(theta)) + seedX, int(radius * numpy.sin(theta)) + seedY]  # save locaction

        # Initialize variables, like Friend tag and near edge identifier
        foundFriend = False  # not near other particle
        nearEdge = False  # not near the edge of the field

        # Set an individual walker out, stop if found a 'friend', give up if it reached the edge of the board
        while not foundFriend and not nearEdge:
            # Run the checking/walking function
            locationNew, foundFriend, nearEdge = checkAround(location, squareSize, matrix)

            # Add to the cluster if near a friend
            if foundFriend:
                # current location, replace with 1 and stop
                matrix[location[1]][location[0]] = 1
                addedCount += 1

            # Otherwise, save the location
            else:
                location = locationNew

        if randomWalkersCount == 400000:
            print("CAUTION: had to break the cycle, taking too many iterations")
            completeCluster = True

        # Once it finds a friend and leaves the previous loop, we must check if it
        # is also touching a circular wall. If so, we have a complete cluster
        if foundFriend and nearEdge:
            print("Random walkers in the cluster: ", addedCount)
            completeCluster = True

    plt.title("DLA Cluster", fontsize=20)
    plt.matshow(
        matrix, interpolation="nearest", cmap=colors.ListedColormap(["navy", "white", "navy"])
    )  # plt.cm.Blues) #ocean, Paired
    plt.xlabel("direction, $x$", fontsize=15)
    plt.ylabel("direction, $y$", fontsize=15)
    plt.savefig("images/cluster.png", dpi=200)
    plt.close()
