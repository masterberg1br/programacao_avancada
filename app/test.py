import numpy as np

x = 10
n = 6

def sort_spaced_seed_index(lenght:int, seeds:int):
    # Gets the indices that should be true
    array = np.arange(lenght)  # Create array of "indices" 0 to x
    true_indices = array[np.round(np.linspace(0, len(array) - 1, seeds)).astype(int)] # Evenly space the indices
    result = [False] * x  # Initialize your whole result to "false"
    for i in range(x):
        if i in true_indices:  # Set evenly-spaced values to true in your result based on indices from get_spaced_indices
            result[i] = True

    print("result:", result)

sort_spaced_seed_index(10, 6)
